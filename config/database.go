package config

import (
	"github.com/gobuffalo/envy"
	"log"
)

type DBConfig struct {
	Host     string
	Name     string
	Cluster  string
	User     string
	Password string
}

func LoadDBConfig() DBConfig {
	err := envy.Load()

	if err != nil {
		log.Fatal(err)
	}

	return DBConfig{
		Host:     envy.Get("DB_HOST", "mongodb.net"),
		Name:     envy.Get("DB_NAME", "mongo"),
		Cluster:  envy.Get("DB_CLUS", "mongo"),
		User:     envy.Get("DB_USER", "mongo"),
		Password: envy.Get("DB_PASS", ""),
	}
}
