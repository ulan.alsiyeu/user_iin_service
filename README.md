# user_iin_service



## Web-сервис для тестового задания

- [ ] Ссылка на сервис - https://user-iin-service.herokuapp.com/  


## Эндпоинты:

1. Проверка ИИН - GET - https://user-iin-service.herokuapp.com/iin_check/{номер_ИИН}

2. Заполнение БД - POST - https://user-iin-service.herokuapp.com/people/info

3. Получение ранее сохраненных данных о человеке по ИИН - GET - https://user-iin-service.herokuapp.com/people/info/iin/{номер_ИИН}

4. Получение ранее сохраненных данных о человеке по части имени (в задании указан эндпоинт с phone, заменил его на name) - GET - https://user-iin-service.herokuapp.com/people/info/name/{часть_имени_или_фамилии}
