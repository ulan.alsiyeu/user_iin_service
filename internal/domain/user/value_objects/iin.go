package value_objects

import (
	"errors"
	"regexp"
	"strconv"
	"time"
)

const (
	sexMan   = "male"
	sexWoman = "female"
)

var (
	errIinFormat = "incorrect IIN format"
	errBirthDate = "date of birth in your IIN is incorrect"
)

var isIinFormatCorrect = regexp.MustCompile(`^[0-9]\d{5}[1-6][0-9]\d{4}$`).MatchString

type Iin struct {
	Value     string
	Sex       string
	BirthDate string
}

func NewIin(value string) (Iin, error) {
	if !isIinFormatCorrect(value) {
		return Iin{}, errors.New(errIinFormat)
	}

	sexDigit, _ := strconv.Atoi(string(value[6]))

	sex := getSex(sexDigit)

	birthDate, err := getBirthDate(value, sexDigit)

	if err != nil {
		return Iin{}, err
	}

	return Iin{
		Value:     value,
		Sex:       sex,
		BirthDate: birthDate,
	}, nil
}

func getSex(sexDigit int) (sex string) {
	if sexDigit%2 == 0 {
		sex = sexWoman
	} else {
		sex = sexMan
	}

	return
}

func getBirthDate(iin string, sexDigit int) (string, error) {
	date := iin[:6]
	today := time.Now()

	switch {
	case sexDigit == 1 || sexDigit == 2:
		date = "18" + date
	case sexDigit == 3 || sexDigit == 4:
		date = "19" + date
	case sexDigit == 5 || sexDigit == 6:
		date = "20" + date
	}

	birthDate, _ := time.Parse("20060102", date)

	if birthDate.After(today) || birthDate.Before(today.AddDate(-200, 0, 0)) {
		return "", errors.New(errBirthDate)
	}

	birthDateStr := birthDate.Format("02.01.2006")

	return birthDateStr, nil
}
