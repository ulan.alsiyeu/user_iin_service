package value_objects

import (
	"errors"
	"fmt"
	"regexp"
	"strings"
	"unicode/utf8"
)

const maxNameLength = 50

var (
	errNameMaxLength = "maximum length of a name is %d"
	errNameContent   = "name should consist only letters"
)
var isAlpha = regexp.MustCompile(`^[A-Za-zА-Яа-яәғқңөұүһіӘҒҚҢӨҰҮҺІёЁ ]+$`).MatchString

type Name string

func NewName(value string) (Name, error) {
	value = strings.Trim(value, " ")

	if utf8.RuneCountInString(value) > maxNameLength {
		return "", fmt.Errorf(errNameMaxLength, maxNameLength)
	}

	if !isAlpha(value) {
		return "", errors.New(errNameContent)
	}

	return Name(value), nil
}

func (n Name) String() string {
	return string(n)
}
