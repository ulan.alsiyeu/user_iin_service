package value_objects

import (
	"errors"
	"regexp"
	"strings"
)

var errPhoneNumberFormat = "incorrect phone number format"

var isPhoneNumber = regexp.MustCompile(`^\+?[1-9]\d{1,14}$`).MatchString

type Phone string

func NewPhone(value string) (Phone, error) {
	value = strings.Trim(value, " ")

	if !isPhoneNumber(value) {
		return "", errors.New(errPhoneNumberFormat)
	}

	return Phone(value), nil
}

func (p Phone) String() string {
	return string(p)
}
