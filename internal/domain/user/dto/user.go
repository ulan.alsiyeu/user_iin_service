package dto

import vo "gitlab.com/ulan.alsiyeu/user_iin_service/internal/domain/user/value_objects"

type UserDTO struct {
	Name  vo.Name
	Iin   vo.Iin
	Phone vo.Phone
}
