package user

import (
	vo "gitlab.com/ulan.alsiyeu/user_iin_service/internal/domain/user/value_objects"
	"golang.org/x/net/context"
)

type Repository interface {
	Create(ctx context.Context, user *User) error
	GetByIin(ctx context.Context, iin vo.Iin) (*User, error)
	GetByNamePart(ctx context.Context, iin vo.Name) (*[]User, error)
}
