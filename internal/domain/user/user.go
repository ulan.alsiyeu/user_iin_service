package user

import (
	"gitlab.com/ulan.alsiyeu/user_iin_service/internal/domain/user/dto"
	vo "gitlab.com/ulan.alsiyeu/user_iin_service/internal/domain/user/value_objects"
)

type User struct {
	Name  vo.Name  `json:"name"`
	Iin   string   `json:"iin"`
	Phone vo.Phone `json:"phone"`
}

func NewUser(d *dto.UserDTO) *User {
	return &User{
		Name:  d.Name,
		Iin:   d.Iin.Value,
		Phone: d.Phone,
	}
}
