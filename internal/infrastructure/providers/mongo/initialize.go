package mongo

import (
	"fmt"
	cfg "gitlab.com/ulan.alsiyeu/user_iin_service/config"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"golang.org/x/net/context"
	"time"
)

func Initialize() (*mongo.Client, error) {
	config := cfg.LoadDBConfig()

	URI := fmt.Sprintf("mongodb+srv://%s:%s@%s.%s/?retryWrites=true&w=majority", config.User, config.Password, config.Cluster, config.Host)
	serverAPIOptions := options.ServerAPI(options.ServerAPIVersion1)

	clientOptions := options.Client().ApplyURI(URI).SetServerAPIOptions(serverAPIOptions)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)

	defer cancel()

	client, err := mongo.Connect(ctx, clientOptions)

	if err != nil {
		return nil, err
	}

	return client, nil
}
