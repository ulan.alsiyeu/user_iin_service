package http_server

import (
	"github.com/gin-gonic/gin"
	"github.com/gobuffalo/envy"
	"net/http"
)

const (
	HTTPServerAddrEnv     = "PORT"
	DefaultHTTPServerAddr = "8080"
)

type HTTPServerConfig struct {
	HTTPServerAddr string
	Router         *gin.Engine
}

type Server interface {
	ListenAndServe() (err error)
}

func InitializeHTTPServerConfig(router *gin.Engine) *HTTPServerConfig {
	return &HTTPServerConfig{
		HTTPServerAddr: ":" + envy.Get(HTTPServerAddrEnv, DefaultHTTPServerAddr),
		Router:         router,
	}
}

func InitializeHTTPServer(cfg *HTTPServerConfig) Server {
	return &http.Server{Addr: cfg.HTTPServerAddr, Handler: cfg.Router}
}
