package router

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/ulan.alsiyeu/user_iin_service/internal/infrastructure/dependencies"
	"gitlab.com/ulan.alsiyeu/user_iin_service/internal/ports/api/v1/controllers/user"
)

func Initialize(container *dependencies.Container) *gin.Engine {
	r := gin.New()

	controller := user.NewController(container.UsersService)

	controller.DefineRoutes(r)

	return r
}
