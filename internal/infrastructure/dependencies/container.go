package dependencies

import (
	"gitlab.com/ulan.alsiyeu/user_iin_service/internal/infrastructure/repositories"
	"gitlab.com/ulan.alsiyeu/user_iin_service/internal/ports/api/v1/controllers/user"
)

type Container struct {
	UsersRepository *repositories.Users
	UsersService    user.Service
}
