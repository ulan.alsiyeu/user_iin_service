//go:generate wire
//go:build wireinject
// +build wireinject

package infrastructure

import (
	"github.com/google/wire"
	services2 "gitlab.com/ulan.alsiyeu/user_iin_service/internal/application/services"
	"gitlab.com/ulan.alsiyeu/user_iin_service/internal/domain/user"
	"gitlab.com/ulan.alsiyeu/user_iin_service/internal/domain/user/services"
	"gitlab.com/ulan.alsiyeu/user_iin_service/internal/infrastructure/dependencies"
	"gitlab.com/ulan.alsiyeu/user_iin_service/internal/infrastructure/providers/http_server"
	"gitlab.com/ulan.alsiyeu/user_iin_service/internal/infrastructure/providers/mongo"
	"gitlab.com/ulan.alsiyeu/user_iin_service/internal/infrastructure/providers/router"
	"gitlab.com/ulan.alsiyeu/user_iin_service/internal/infrastructure/repositories"
)

func BuildApplication() (*Application, error) {
	wire.Build(
		router.Initialize,
		mongo.Initialize,
		repositories.InitializeUsersRepository,
		services2.InitializeUsersService,

		wire.Bind(new(user.Repository), new(*repositories.Users)),
		wire.Bind(new(services.Service), new(*services2.Users)),

		http_server.InitializeHTTPServerConfig,
		http_server.InitializeHTTPServer,

		wire.Struct(new(dependencies.Container), "*"),
		wire.Struct(new(Application), "HttpServer", "container"),
	)

	return &Application{}, nil
}
