package repositories

import (
	"github.com/gobuffalo/envy"
	"github.com/pkg/errors"
	"gitlab.com/ulan.alsiyeu/user_iin_service/internal/domain/user"
	vo "gitlab.com/ulan.alsiyeu/user_iin_service/internal/domain/user/value_objects"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"golang.org/x/net/context"
	"log"
	"regexp"
)

const collectionName = "users"

type Users struct {
	mongoClient *mongo.Client
}

func (u *Users) Create(ctx context.Context, user *user.User) error {
	collection := u.getCollection()

	_, err := collection.InsertOne(
		ctx,
		bson.D{{"name", user.Name}, {"iin", user.Iin}, {"phone", user.Phone}},
	)
	if err != nil {
		return err
	}

	return nil
}

func (u *Users) GetByIin(ctx context.Context, iin vo.Iin) (*user.User, error) {
	var userEntity user.User
	collection := u.getCollection()

	err := collection.FindOne(ctx, bson.D{{"iin", iin.Value}}).Decode(&userEntity)

	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, err
		}
		log.Fatal(err)
	}

	return &userEntity, nil
}

func (u *Users) GetByNamePart(ctx context.Context, name vo.Name) (*[]user.User, error) {
	var users []user.User
	collection := u.getCollection()

	cursor, err := collection.Find(ctx, bson.D{{"name", primitive.Regex{Pattern: regexp.QuoteMeta(name.String()), Options: "i"}}})

	if err != nil {
		return nil, err
	}

	if err := cursor.All(ctx, &users); err != nil {
		return nil, err
	}

	if len(users) > 0 {
		return &users, nil
	}

	return nil, errors.New("no documents with this name part")
}

func (u *Users) getCollection() *mongo.Collection {
	return u.mongoClient.Database(envy.Get("DB_NAME", "mongo")).Collection(collectionName)
}

func InitializeUsersRepository(mongoClient *mongo.Client) *Users {
	return &Users{mongoClient}
}
