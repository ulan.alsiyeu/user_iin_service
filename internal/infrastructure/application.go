package infrastructure

import (
	"github.com/gobuffalo/envy"
	"gitlab.com/ulan.alsiyeu/user_iin_service/internal/infrastructure/dependencies"
	"gitlab.com/ulan.alsiyeu/user_iin_service/internal/infrastructure/providers/http_server"
	"golang.org/x/net/context"
	"os"
)

type Application struct {
	HttpServer http_server.Server
	container  *dependencies.Container
}

func InitializeApplication() (*Application, error) {
	if err := initializeTimeZone(); err != nil {
		return nil, err
	}

	app, err := BuildApplication()

	if err != nil {
		return nil, err
	}

	return app, nil
}

func initializeTimeZone() error {
	return os.Setenv("TZ", envy.Get("APP_TIMEZONE", "UTC"))
}

func (a *Application) Start(ctx context.Context) error {
	err := a.HttpServer.ListenAndServe()
	if err != nil {
		return err
	}

	return nil
}
