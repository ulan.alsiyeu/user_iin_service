package cmd

import (
	"gitlab.com/ulan.alsiyeu/user_iin_service/internal/infrastructure"
	"golang.org/x/net/context"
	"log"
)

func Execute() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	log.Println("Starting server")

	application, err := infrastructure.InitializeApplication()

	if err != nil {
		log.Fatal("Can not initialize application")
	}

	err = application.Start(ctx)

	if err != nil {
		log.Fatal("Application failed")
	}

	log.Println("Finished")
}
