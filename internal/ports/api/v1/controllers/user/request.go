package user

import (
	"errors"
	"github.com/gin-gonic/gin"
	"gitlab.com/ulan.alsiyeu/user_iin_service/internal/domain/user/dto"
	vo "gitlab.com/ulan.alsiyeu/user_iin_service/internal/domain/user/value_objects"
)

type IinRequest struct {
	Iin string `uri:"iin" binding:"required"`
}

type CreateUserRequest struct {
	Name  string `form:"name" binding:"required"`
	Iin   string `form:"iin" binding:"required"`
	Phone string `form:"phone" binding:"required"`
}

type UsersByNamePartRequest struct {
	NamePart string `uri:"name" binding:"required"`
}

func GetIinRequest(ctx *gin.Context) (*IinRequest, error) {
	r := &IinRequest{}

	if err := ctx.ShouldBindUri(r); err != nil {
		return nil, errors.New("request parameter iin is invalid")
	}

	return r, nil
}

func GetCreateUserRequest(ctx *gin.Context) (*CreateUserRequest, error) {
	r := &CreateUserRequest{}

	if err := ctx.ShouldBind(r); err != nil {
		return nil, errors.New("request body parameters are invalid")
	}

	return r, nil
}

func GetUsersByNamePartRequest(ctx *gin.Context) (*UsersByNamePartRequest, error) {
	r := &UsersByNamePartRequest{}

	if err := ctx.ShouldBindUri(r); err != nil {
		return nil, errors.New("request parameter name is invalid")
	}

	return r, nil
}

func (c *CreateUserRequest) GetUserDTO() (*dto.UserDTO, error) {
	name, err := vo.NewName(c.Name)

	if err != nil {
		return nil, err
	}

	iin, err := vo.NewIin(c.Iin)

	if err != nil {
		return nil, err
	}

	phone, err := vo.NewPhone(c.Phone)

	if err != nil {
		return nil, err
	}

	return &dto.UserDTO{
		Name:  name,
		Iin:   iin,
		Phone: phone,
	}, nil
}
