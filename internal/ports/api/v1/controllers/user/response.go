package user

import (
	"github.com/gin-gonic/gin"
	vo "gitlab.com/ulan.alsiyeu/user_iin_service/internal/domain/user/value_objects"
)

type IinSuccessResponse struct {
	Correct   bool   `json:"correct"`
	Sex       string `json:"sex"`
	BirthDate string `json:"date_of_birth"`
}

func GetIinSuccessResponse(iin vo.Iin) *IinSuccessResponse {
	return &IinSuccessResponse{
		Correct:   true,
		Sex:       iin.Sex,
		BirthDate: iin.BirthDate,
	}
}

func GetIinErrorResponse() gin.H {
	return gin.H{"correct": false}
}

func GetCreateUserSuccessResponse() gin.H {
	return gin.H{"success": true}
}

func GetCreateUserErrorResponse(err error) gin.H {
	return gin.H{"success": false, "error": err.Error()}
}
