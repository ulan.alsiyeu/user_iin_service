package user

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/ulan.alsiyeu/user_iin_service/internal/domain/user"
	"gitlab.com/ulan.alsiyeu/user_iin_service/internal/domain/user/dto"
	vo "gitlab.com/ulan.alsiyeu/user_iin_service/internal/domain/user/value_objects"
	"golang.org/x/net/context"
	"net/http"
)

type Service interface {
	CreateUser(ctx context.Context, userDto *dto.UserDTO) error
	GetUserInfoById(ctx context.Context, iin vo.Iin) (*user.User, error)
	GetUsersInfoByNamePart(ctx context.Context, name vo.Name) (*[]user.User, error)
}

type Controller struct {
	UserService Service
}

func NewController(userService Service) *Controller {
	return &Controller{UserService: userService}
}

func (c *Controller) CheckIin(ctx *gin.Context) {
	r, err := GetIinRequest(ctx)

	if err != nil {
		ctx.JSON(http.StatusOK, GetIinErrorResponse())
		return
	}

	iin, err := vo.NewIin(r.Iin)

	if err != nil {
		ctx.JSON(http.StatusOK, GetIinErrorResponse())
		return
	}

	ctx.JSON(http.StatusOK, GetIinSuccessResponse(iin))
}

func (c *Controller) CreateUser(ctx *gin.Context) {
	r, err := GetCreateUserRequest(ctx)

	if err != nil {
		ctx.JSON(http.StatusInternalServerError, GetCreateUserErrorResponse(err))
		return
	}

	dt, err := r.GetUserDTO()

	if err != nil {
		ctx.JSON(http.StatusInternalServerError, GetCreateUserErrorResponse(err))
		return
	}

	err = c.UserService.CreateUser(ctx, dt)

	if err != nil {
		ctx.JSON(http.StatusInternalServerError, GetCreateUserErrorResponse(err))
		return
	}

	ctx.JSON(http.StatusOK, GetCreateUserSuccessResponse())
}

func (c *Controller) GetUserById(ctx *gin.Context) {
	r, err := GetIinRequest(ctx)

	if err != nil {
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}

	iin, err := vo.NewIin(r.Iin)

	if err != nil {
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}

	userEntity, err := c.UserService.GetUserInfoById(ctx, iin)

	if err != nil {
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}

	ctx.JSON(http.StatusOK, userEntity)
}

func (c *Controller) GetUsersByNamePart(ctx *gin.Context) {
	r, err := GetUsersByNamePartRequest(ctx)

	if err != nil {
		ctx.JSON(http.StatusOK, []user.User{})
		return
	}

	namePart, err := vo.NewName(r.NamePart)

	if err != nil {
		ctx.JSON(http.StatusOK, []user.User{})
		return
	}

	users, err := c.UserService.GetUsersInfoByNamePart(ctx, namePart)

	if err != nil {
		ctx.JSON(http.StatusOK, []user.User{})
		return
	}

	ctx.JSON(http.StatusOK, users)
}

func (c *Controller) DefineRoutes(r gin.IRouter) {
	r.GET("/iin_check/:iin", c.CheckIin)
	r.POST("/people/info", c.CreateUser)
	r.GET("/people/info/iin/:iin", c.GetUserById)
	r.GET("/people/info/name/:name", c.GetUsersByNamePart)
}
