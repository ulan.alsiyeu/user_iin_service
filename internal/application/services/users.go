package services

import (
	"errors"
	"gitlab.com/ulan.alsiyeu/user_iin_service/internal/domain/user"
	"gitlab.com/ulan.alsiyeu/user_iin_service/internal/domain/user/dto"
	vo "gitlab.com/ulan.alsiyeu/user_iin_service/internal/domain/user/value_objects"
	"golang.org/x/net/context"
)

type Users struct {
	UserRepository user.Repository
}

func (u *Users) CreateUser(ctx context.Context, userDto *dto.UserDTO) error {
	userEntity, _ := u.UserRepository.GetByIin(ctx, userDto.Iin)

	if userEntity != nil {
		return errors.New("user with this IIN already exists")
	}

	userEntity = user.NewUser(userDto)
	err := u.UserRepository.Create(ctx, userEntity)

	if err != nil {
		return err
	}

	return nil
}

func (u *Users) GetUserInfoById(ctx context.Context, iin vo.Iin) (*user.User, error) {
	userEntity, err := u.UserRepository.GetByIin(ctx, iin)

	if err != nil {
		return nil, err
	}

	return userEntity, nil
}

func (u *Users) GetUsersInfoByNamePart(ctx context.Context, namePart vo.Name) (*[]user.User, error) {
	users, err := u.UserRepository.GetByNamePart(ctx, namePart)

	if err != nil {
		return nil, err
	}

	return users, nil
}

func InitializeUsersService(userRepository user.Repository) *Users {
	return &Users{UserRepository: userRepository}
}
